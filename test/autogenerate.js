const assert = require('assert');
const rp = require('request-promise');
const filepaths = require('filepaths');
const rsync = require('sync-request');

const config = require('./../config');
const createServer = require('../src/server');

const API_URL = 'http://0.0.0.0:3030';
const AUTH_USER = { login: 'pupkin@gmail.com', pass: '12345' };
const AUTH_GGROUP = { api_key: '00:dd:fa:de', name: 'autotest-gate', pub_key: '---fake---key' };

const customExamples = {
  'string': 'abc',
  'number': 2,
  'boolean': true,
  'any': null,
  'date': new Date()
};

const allowedStatusCodes = {
  200: true,
  404: true
};

const requestTimeout = 15000;

function getExampleValue(joiObj) {
  if( joiObj == null ) // if joi is null
    return joiObj;

  if( typeof(joiObj) != 'object' ) //If it's not joi object
    return joiObj;

  if( typeof(joiObj._examples) == 'undefined' )
    return customExamples[ joiObj._type ];

  if( joiObj._examples.length <= 0 )
    return customExamples[ joiObj._type ];

  return joiObj._examples[ 0 ].value;
}

function generateQuiryParams(queryObject) {
  let queryArray = [];
  for(let name in queryObject)
    queryArray.push(`${name}=${queryObject[name]}`);

  return queryArray.join('&');
}

function generatePath(basicPath, paramsScheme) {
  let result = basicPath;

  if( !paramsScheme )
    return result;

  let replaces = generateJOIObject(paramsScheme);

  for(let key in replaces)
    result = result.replace(`{${key}}`, replaces[ key ]);

  return result;
}

function genAuthHeaders() {
  let result = {};

  let respToken = rsync('POST', API_URL + `/api/auth`, {
    json: {
      email: AUTH_USER.login,
      password: AUTH_USER.pass
    }
  });
  let respTokenBody = JSON.parse(respToken.getBody('utf8'));
  
  result[ 'token' ] = {
    Authorization: 'Bearer ' + respTokenBody.data[ 0 ].token
  };
  
  return result;
}

function generateJOIObject(schema) {
  let result = {};
  
  if( schema.type !== 'object' ) {
    return result;
  }
  
  const iterator = schema._ids._byKey.entries();
  let item;
  
  while(item = iterator.next().value) {    
    if( item[ 1 ].schema.type == 'object' ) {
      result[ item[ 1 ].id ] = generateJOIObject(item[1].schema);
      continue;
    }
    
    if( item[1].schema.type == 'array' ) {      
      let items = item[1].schema[ '$_terms' ].items;      
      result[ item[1].id ] = [];
      
      if( items.length ) {        
        for(let subItem of items) {
          result[ item[1].id ].push(generateJOIObject(subItem));
        }
      }

      continue;
    }
    
    let examples = item[1].schema[ '$_terms' ].examples;
    
    if( examples && examples.length ) {
      result[ item[1].id ] = examples[ 0 ];
      continue;
    }
    result[ item[1].id ] = customExamples[ item[1].schema.type ];
  }
  
  return result;
}

function generateRequest(route, authKeys) {  
  if( !route.options.validate ) {
    return {
      method: route.method,
      url: API_URL + generatePath(route.path, {}) + '?' + generateQuiryParams( generateJOIObject({}) ),
      headers: authKeys[ route.options.auth ]  ? authKeys[ route.options.auth ] : {},
      json: true,
      timeout: requestTimeout
    }
  }
  
  return {
    method: route.method,
    url: API_URL + generatePath(route.path, route.options.validate.params) + '?' + generateQuiryParams( generateJOIObject(route.options.validate.query || {}) ),
    headers: authKeys[ route.options.auth ]  ? authKeys[ route.options.auth ] : {},
    body: generateJOIObject(route.options.validate.payload || {}),
    json: true,
    timeout: requestTimeout
  }
}

let authKeys = genAuthHeaders();
let testSec = [ 'POST', 'PUT', 'GET', 'DELETE' ];
let routeList = [];

for(let route of filepaths.getSync(__dirname + '/../src/routes/'))
  routeList.push(require(route));

describe('Autogenerate Hapi Routes TEST', async () => {

  for(let metod of testSec)
  for(let testRoute of routeList) {
    if( testRoute.method != metod ) { 
      continue;
    }
    
    if( testRoute.options.tags && testRoute.options.tags.indexOf('noTest') > -1 ) {
      continue;
    }

    it(`TESTING: ${testRoute.method} ${testRoute.path}`,  async function () {
      let options = generateRequest(testRoute, authKeys);
      if( !options ) return assert.ok(true);

      let statusCode = 0;

      try {
        let result = await rp( options );
        statusCode = 200;
      } catch(err) {
        statusCode = err.statusCode;
      }

      if( !allowedStatusCodes[ statusCode ] ) {
        console.log('*** DEBUG STACK FOR:', `${testRoute.method} ${testRoute.path}`);
        console.log('options:', options);
        console.log('StatusCode:', statusCode);
      }

      return assert.ok(allowedStatusCodes[ statusCode ]);
    });

  }

});
