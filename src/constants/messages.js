
const statuses = {
  UNREAD: { id: 0, name: 'Unread', color: 'gray' },
  READ: { id: 1, name: 'Read', color: 'blue' }
};

const statusesById = [];
for(let index in statuses)
  statusesById[ statuses[ index ].id ] = statuses[ index ];

module.exports = {
  statuses,
  statusesById
};
