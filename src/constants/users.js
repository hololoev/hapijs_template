const roles = {
  SUPERADMIN: { id: 1, name: 'Superadmin', color: 'red' },
  ADMIN: { id: 2, name: 'Admin', color: 'yellow' },
  USER: { id: 3, name: 'User', color: 'green' }
}; 

const statuses = {
  ENABLED: { id: 1, name: 'Enabled', color: 'green' },
  DISABLED: { id: 0, name: 'Disabled', color: 'red' }
};

const rolesById = [];
for(let index in roles)
  rolesById[ roles[ index ].id ] = roles[ index ];

const statusesById = [];
for(let index in statuses)
  statusesById[ statuses[ index ].id ] = statuses[ index ];

module.exports = {
  roles, statuses,
  rolesById, statusesById
};
