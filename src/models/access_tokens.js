
const Crypto = require('crypto');

module.exports = (sequelize, DataTypes) => {
  const accessToken = sequelize.define('access_tokens', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    token: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    expiresAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
  });
  
  accessToken.generateAccessTokenString = () => {
    // eslint-disable-next-line no-undef
    return Crypto.createHmac('md5', Crypto.randomBytes(512).toString()).update([].slice.call(arguments).join(':')).digest('hex');
  };
  
  accessToken.createAccessToken = (user) => {
    const options = {
      user_id: user.get('id'),
      expiresAt: (new Date(new Date().valueOf() + (30 * 24 * 60 * 60 * 1000))),
      token: accessToken.generateAccessTokenString(user.get('id'), user.get('email'), new Date().valueOf())
    };    
    return accessToken.create(options);
  };
  
  accessToken.associate = function(models) {
    accessToken.belongsTo(models.users, { as: 'user', foreignKey: 'user_id', targetKey: 'id' });
  };
  
  accessToken.dummyData = require('./accessToken.json');
  
  return accessToken;
};
