
const Bcrypt = require('bcryptjs');

const constants = require('../constants/users');

const bcryptRounds = 10;

module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING
    },
    role: {
      type: DataTypes.INTEGER
    },
    status: {
      type: DataTypes.INTEGER
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      set (val) {
        this.setDataValue('password', Bcrypt.hashSync(val, bcryptRounds));
      }
    }
  });

  users.verifyPassword = function (unencrypted, enctrypted) { 
    return Bcrypt.compare(unencrypted, enctrypted);
  };

  users.dummyData = require('./users.json');
  
  Object.assign(users, constants);

  return users;
};
