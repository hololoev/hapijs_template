
const constants = require('../constants/messages');

module.exports = (sequelize, DataTypes) => {
  const messages = sequelize.define('messages', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    sender_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    recipient_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    viewDate: {
      type: DataTypes.DATE
    },
    message: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  });

  messages.dummyData = require('./messages.json');
  
  Object.assign(messages, constants);

  return messages;
};
