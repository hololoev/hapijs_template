
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const basicSchemes = require('../../libs/schemes/basic');
const commonSchemes = require('../../libs/schemes/common');
const buildResult = require('../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');
  const messages = request.getModel(request.server.config.db.database, 'messages');
  
  let recipient = await users.findOne({ where: { id: request.payload.recipient_id } });
  if( !recipient ) throw Boom.notFound('Recipient not found');
  
  let newItem = Object.assign({}, request.payload);
  newItem.status = messages.statuses.UNREAD.id;
  newItem.sender_id = request.auth.artifacts.user.id;
  
  let newMessage = await messages.create(newItem);
  
  return buildResult([newMessage.dataValues]);
}

module.exports = {
  method: 'POST',
  path: '/api/messages',
  options: {
    handler: response,
    description: 'Create message',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      payload: Joi.object({
        recipient_id: Joi.number().integer().required().example(1),
        message: Joi.string().min(1).max(100).required().example('Lorem ipsum')
      })
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.messageSchema) }
  }
};
