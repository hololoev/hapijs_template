
const Op = require('sequelize').Op;

const queryHelper = require('../../libs/queryHelper');
const basicSchemes = require('../../libs/schemes/basic');
const commonSchemes = require('../../libs/schemes/common');
const buildResult = require('../../libs/helpers').buildResult;

async function response(request) {
  //const sequelize = request.getDb().sequelize;
  const messages = request.getModel(request.server.config.db.database, 'messages');
  
  let query = queryHelper.parseQueryParams(request.query);
  query.where[ Op.or ] = [
    { sender_id: request.auth.artifacts.user.id },
    { recipient_id: request.auth.artifacts.user.id }
  ];

  let result = await messages.findAll(query);
  result = result.map(el => el.get({ plain: true }));
  let totalCount = await messages.count(query);
  
  return buildResult(result, totalCount, result.length, request.query.__offset);
}

module.exports = {
  method: 'GET',
  path: '/api/messages',
  options: {
    handler: response,
    description: 'Get user messages',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      query: commonSchemes.defaultQueryScheme
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.messageScheme) }
  }
};
