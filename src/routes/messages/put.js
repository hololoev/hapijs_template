
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const basicSchemes = require('../../libs/schemes/basic');
const commonSchemes = require('../../libs/schemes/common');
const buildResult = require('../../libs/helpers').buildResult;

async function response(request) {
  const messages = request.getModel(request.server.config.db.database, 'messages');
  
  let curMessage = await messages.findOne({
    where: {
      id: request.params.id,
      recipient_id: request.auth.artifacts.user.id
    }
  });
  
  if( !curMessage )
    throw Boom.notFound('Message not found');
  
  await messages.update({
    status: messages.statuses.READ.id,
    viewDate: new Date()
  }, {
    where: {
      id: request.params.id,
      recipient_id: request.auth.artifacts.user.id
    }
  });
  
  curMessage = await messages.findOne({
    where: {
      id: request.params.id,
      recipient_id: request.auth.artifacts.user.id
    }
  });
  
  return buildResult([curMessage.dataValues]);
}

module.exports = {
  method: 'PUT',
  path: '/api/messages/{id}',
  options: {
    handler: response,
    description: 'Update message as read',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        id: Joi.number().integer().description('Message id').example(1)
      })
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.messageSchema) }
  }
};
