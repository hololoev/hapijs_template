
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');
const Op = require('sequelize').Op;

const basicSchemes = require('../../libs/schemes/basic');
const commonSchemes = require('../../libs/schemes/common');
const buildResult = require('../../libs/helpers').buildResult;

async function response(request) {
  const messages = request.getModel(request.server.config.db.database, 'messages');
  let curMessage = await messages.findOne({
    where: {
      id: request.params.id, 
      [ Op.or ]: [
        { sender_id: request.auth.artifacts.user.id },
        { recipient_id: request.auth.artifacts.user.id }
      ]
    }
  });

  if ( !curMessage ) throw Boom.notFound('Message not found');

  return buildResult([ curMessage ]);
}

module.exports = {
  method: 'GET',
  path: '/messages/{id}',
  options: {
    handler: response,
    description: 'Get single message by id',
    tags: [ 'api', 'noTest' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        id: Joi.number().integer().required().example(1)
      })
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.messageScheme) }
  }
};
