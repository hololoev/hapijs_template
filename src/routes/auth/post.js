const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const basicSchemes = require('../../libs/schemes/basic');
const commonSchemes = require('../../libs/schemes/common');
const buildResult = require('../../libs/helpers').buildResult;

async function response(request) {
  const accessTokens = request.getModel(request.server.config.db.database, 'access_tokens');
  const users = request.getModel(request.server.config.db.database, 'users');

  let userRecord = await users.findOne({ where: { email: request.payload.email } });
  if ( !userRecord ) {
    throw Boom.unauthorized(); 
  }

  if ( !await users.verifyPassword(request.payload.password, userRecord.password) ) {
    throw Boom.unauthorized();
  }

  let token = await accessTokens.createAccessToken(userRecord);
  let result = await accessTokens.findAll({
    where: { id: token.id },
    include: [
      {
        model: users,
        as: 'user',
        attributes: [ 'id', 'name', 'email', 'role', 'status', 'updatedAt', 'createdAt' ]
      }
    ]
  });

  result = result.map(el => el.get({ plain: true }));
  return buildResult(result);
}

const responseScheme = basicSchemes.tokenScheme.keys({
  user: basicSchemes.userScheme
});

module.exports = {
  method: 'POST',
  path: '/api/auth',
  options: {
    handler: response,
    description: 'Create auth token',
    tags: [ 'api', 'noTest' ],
    validate: {
      payload: Joi.object({
        email: Joi.string().required().example('pupkin@gmail.com'),
        password: Joi.string().required().example('12345')
      })
    },
    response: { schema: commonSchemes.responseWrapper(responseScheme) }
  }
};
