'use strict';

const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const filepaths = require('filepaths');
const Sequelize = require('sequelize');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const AuthBearer = require('hapi-auth-bearer-token');
const hapiBoomDecorators = require('hapi-boom-decorators');
const bearerValidation = require('./libs/bearerValidation');

const logger = require('./libs/logger');

const config = require('../config');
const Package = require('../package');

const swaggerOptions = {
  jsonPath: '/api/documentation.json',
  documentationPath: '/api/documentation',
  info: {
    title: Package.description,
    version: Package.version
  },
};

async function createServer() {
  const server = await new Hapi.Server(config.server);

  await server.register([
    AuthBearer,
    hapiBoomDecorators,
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    },
    {
      plugin: require('hapi-sequelizejs'),
      options: [
        {
          name: config.db.database, 
          models: [ __dirname + '/models/*.js '], // Путь к моделькам
          //ignoredModels: [__dirname + '/server/models/**/*.js'], // if need to ignore some models
          sequelize: new Sequelize(config.db),
          sync: true,
          forceSync: false,
        },
      ],
    }
  ]);
  
  server.auth.strategy('token', 'bearer-access-token', {
    allowQueryToken: false,
    unauthorized: bearerValidation.unauthorized,
    validate: bearerValidation.validate
  });

  // Load routes
  let routes = filepaths.getSync(__dirname + '/routes/');
  for (let route of routes) {
    server.route( require(route) );
  }
  
  server.ext({
    type: 'onRequest',
    method: async function (request, h) {
      request.server.config = Object.assign({}, config);
      return h.continue;
    }
  });
  
  server.events.on('response', (request) => {
    logger.route(request);
  });
  
  // Start server
  try {
    await server.start();
    logger.info(`Server running at: ${server.info.uri}`);
  } catch (err) {
    logger.error(err);
    process.exit(1);    
  }

  return server;
}

module.exports = createServer;
