
const Joi = require('@hapi/joi');

const tokenScheme = Joi.object({
  id: Joi.number().integer().example(1),
  token: Joi.string().example('4443655c28b42a4349809accb3f5bc71'),
  user_id: Joi.number().integer().example(2),
  updatedAt: Joi.date().example(new Date()),
  createdAt: Joi.date().example(new Date()),
  expiresAt: Joi.date().example(new Date()),
});

const userScheme = Joi.object({
  id: Joi.number().integer().example(1),
  name: Joi.string(),
  email: Joi.string(),
  status: Joi.number().integer(),
  role: Joi.number().integer(),
  updatedAt: Joi.date().example(new Date()),
  createdAt: Joi.date().example(new Date())
});

const messageSchema = Joi.object({
  id: Joi.number().integer().example(1),
  recipient_id: Joi.number().integer(),
  sender_id: Joi.number().integer(),
  status: Joi.number().integer().example(0),
  viewDate: Joi.date().example(new Date()),
  message: Joi.string().example('Lorem ipsum'),
  updatedAt: Joi.date().example(new Date()),
  createdAt: Joi.date().example(new Date())
});

module.exports = {
  tokenScheme,
  userScheme,
  messageSchema
};
