
const appName = 'hapi-server-template';

function sql(sql) {
  // eslint-disable-next-line no-console
  console.log(JSON.stringify({
    app: appName,
    type: 'sql',
    sql: sql
  }));
}

function route(request) {
  // eslint-disable-next-line no-console
  console.log(JSON.stringify({
    app: appName,
    type: 'request',
    method: request.route.method,
    url: request._route.path,
    statusCode: request.raw.res.statusCode,
    remoteAddr: request.headers[ 'x-real-ip' ],
    referrer: request.info.referrer,
    query: request.query,
    params: request.params,
    payload: request.payload
  }));
}

function error(error) {
  // eslint-disable-next-line no-console
  console.log(JSON.stringify({
    app: appName,
    type: 'error',
    message: error.toString()
  }));
}

function info(message) {
  // eslint-disable-next-line no-console
  console.log(JSON.stringify({
    app: appName,
    type: 'info',
    message: message
  }));
}

module.exports = { sql, route, error, info };
